- [x] login
- [x] api sign
- [x] 商品列表，分类
- [x] 广告跳转
- [x] 我的地址
  - [x] 用户id使用当前用户id
- [] 支付
  - [] 判断是否选择地址
  - [] 调用微信支付接口
- [] 分享
- [] 反馈
- [] 商品详情 播放视频
- [x] 订单列表
  - [x] 再次购买
  - [x] 取消订单
  - [x] 去支付
  - [x] 确认收货
  - [] 评价订单
  - [] 保存用户信息：头像、昵称
