import Taro, { Component, Config } from '@tarojs/taro'
import { View, Text, Button } from '@tarojs/components'
import { AtAvatar, AtList, AtListItem, AtIcon } from 'taro-ui-hello-red'
import classnames from 'classnames'

import { observer, inject } from '@tarojs/mobx'

// components
import OrderItem from './components/OrderItem'

// store
import store from './store'
import accountStore from '../../store/account'
import feedbacktStore from '../../pages/feedback/store'

import './index.scss'

type DataProps = { store: typeof store; accountStore: typeof accountStore }

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { store, accountStore }
})
@observer
class PersonalPage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '个人',
    backgroundColor: '#F3F2ED',
  }

  componentDidShow() {
    this.props.store.init()
  }


  navigateToOrder = (tabIndex?: number) => {
    Taro.navigateTo({
      url: `/pages/orderList/index?tabIndex=${tabIndex}`,
    })
  }
  navigateToAddress = () => {
    Taro.navigateTo({
      url: '/pages/address/list/index',
    })
  }

  navagateToFeedback = () => {
    feedbacktStore.navigateTo()
  }

  render() {
    const { userAccount } = this.props.accountStore
    const { orderCount } = this.props.store
    return (
      <View className='personal-page'>
        {/* 用户 */}
        <View className='personal-page__user'>
          <AtAvatar className='personal-page__user-avatar' circle image={userAccount ? userAccount.avatar : ''} />
          <Text className='personal-page__user-name'>{userAccount ? userAccount.nickName : ''}</Text>
        </View>
        {/* 我的订单 */}
        <View className='personal-page__orders'>
          <View className='personal-page__orders-title'>
            <Text className='personal-page__orders-title-one'>我的订单</Text>
            <Text className='personal-page__orders-title-two' onClick={this.navigateToOrder.bind(this, 0)}>
              全部订单
            </Text>
          </View>
          <View className='personal-page__orders-content'>
            <OrderItem
              icon={require('./assets/order/pending-pay.png')}
              label='待付款'
              count={orderCount.pendingPay}
              onClick={this.navigateToOrder.bind(this, 1)}
            />
            <OrderItem
              icon={require('./assets/order/pending-delivery.png')}
              label='待收货'
              count={orderCount.pendingReceived}
              onClick={this.navigateToOrder.bind(this, 2)}
            />
            <OrderItem
              icon={require('./assets/order/received.png')}
              label='待评价'
              count={orderCount.pendingComment}
              onClick={this.navigateToOrder.bind(this, 3)}
            />
            <OrderItem
              icon={require('./assets/order/completed.png')}
              label='已完成'
              onClick={this.navigateToOrder.bind(this, 4)}
            />
          </View>
        </View>
        {/* 列表 */}
        <View className='personal-page__list'>
          <AtList>
            <AtListItem title='我的地址' arrow='right' onClick={this.navigateToAddress} />
            <AtListItem title='意见反馈' arrow='right' onClick={this.navagateToFeedback} />
            <View className={classnames('at-list__item', 'my_list-item__button')}>
              <AtIcon className='my_list-item__button-icon' value='chevron-right' />
              <Button openType='share'>分享红品</Button>
            </View>
          </AtList>
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default PersonalPage
