import { View, Image } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

import './index.scss'

export interface DataProps {
  icon: string
  label: string
  count?: number
}
export interface EventProps {
  onClick: () => any
}
export type Props = DataProps & EventProps

interface State {}

class OrderItem extends Component<Props, State> {
  // ---------------- 私有属性 ----------------

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {}
  }

  // ---------------- 生命周期 ----------------

  // ---------------- 计算属性 ----------------

  // ---------------- 方法 ----------------

  render() {
    const { icon, label, count, onClick } = this.props
    return (
      <View className='order-item' onClick={onClick}>
        <View className='order-item__icon'>
          <Image src={icon} />
          {count && count > 0 && <View className='order-item__badge'>{count}</View>}
        </View>
        <View className='order-item__label'>{label}</View>
      </View>
    )
  }
}

export default OrderItem
