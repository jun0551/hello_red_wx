import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// store
import accountStore from '../../store/account'

class PersonalStore {
  @observable
  orderCount = {
    pendingPay: 0, // 待付款
    pendingReceived: 0, // 待收货：实际 待发货 + 待收货
    pendingComment: 0, // 待评价
  }

  @action
  async init() {
    Taro.showLoading({ title: '加载中...' })

    // 加载订单数量
    this.loadOrderCount()
    // 加载个人信息
    await accountStore.loadCurrentUserInfo({ refresh: true })

    Taro.hideLoading()
  }

  async loadOrderCount() {
    const session = await accountStore.getLoginSession()
    const { data } = await orderApi.getCount({ userId: session.uid })
    this.orderCount = {
      pendingPay: data.dfk,
      pendingReceived: data.dfh + data.dsh, // 待发货 + 待收货
      pendingComment: data.dpj,
    }
  }
}

export default new PersonalStore()
