import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as productsInfoApi from '../../apis/productsInfoApi'
import * as evaluateApi from '../../apis/evaluateApi'

// models
import ProductsInfo from '../../models/ProductsInfo'

// store
import accountStore from '../../store/account'

class EvaluationStore {
  orderId: number

  @observable
  product: ProductsInfo

  /**
   * 星数
   */
  @observable
  star: number

  /**
   * 评论内容
   */
  @observable
  details: string

  /**
   * 图片
   */
  @observable
  images: Array<{ url: string }> = []

  @action
  navigateTo({ orderId, productId }: { orderId: number; productId: number }) {
    Taro.navigateTo({
      url: `/pages/evaluation/index?orderId=${orderId}&productId=${productId}`,
    })
  }

  @action
  init = async ({ orderId, productId }: { orderId: number; productId: number }) => {
    this.orderId = orderId
    this.details = ''
    this.star = 0
    this.images = []

    Taro.showLoading({ title: '加载中...' })
    const { data: product } = await productsInfoApi.getById({ id: productId })
    this.product = product

    Taro.hideLoading()
  }

  @action
  changeStar = (star: number) => {
    this.star = star
  }
  @action
  changeDetails = (details: string) => {
    this.details = details
  }

  @action
  submit = async () => {
    if (!this.star) {
      return Taro.showToast({ title: '请评分' })
    }
    Taro.showLoading({ title: '提交中...', mask: true })
    const session = await accountStore.getLoginSession()

    await evaluateApi.evaluate({
      productId: this.product.id,
      userId: session.uid,
      orderId: this.orderId,
      star: this.star,
      details: this.details,
      url: this.images.map(item => item.url).join(','),
    })
    Taro.hideLoading()
    Taro.showToast({ title: '已提交', mask: true, duration: 1500 })

    await new Promise(resolve => setTimeout(resolve, 1500))

    Taro.navigateBack()
  }
}

export default new EvaluationStore()
