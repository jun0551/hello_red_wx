import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'

import { AtRate, AtTextarea, AtButton, AtImagePicker } from 'taro-ui-hello-red'
import { observer, inject } from '@tarojs/mobx'
import toNumber from 'lodash/toNumber'

// components
// import { AtTextarea, AtCheckbox, AtButton } from 'taro-ui-hello-red'

// styles
import './index.scss'

// store
import store from './store'

type DataProps = {
  store: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { store: store }
})
@observer
class EvaluationPage extends Component<Props, State> {
  submit = store.submit
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '评价',
    navigationBarBackgroundColor: '#F2F2F2',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentDidShow() {
    const orderId = toNumber(this.$router.params.orderId)
    const productId = toNumber(this.$router.params.productId)
    this.props.store.init({ orderId, productId })
  }
  handleStartChange = (star: any) => {
    store.changeStar(star)
  }

  handleDetailsChange = (e: any) => {
    store.changeDetails(e.detail.value)
  }

  handleImgsChange = (files: any) => {
    console.info('11', files)
  }

  handleClickImage = (index: number, file: any) => {
    console.info(index, file)
  }

  render() {
    const { product, star, details, images } = this.props.store

    // TODO: product.img
    const img = product && product.imageInfos[0] ? product.imageInfos[0].imgUrl : ''

    return (
      <View className='evaluation-page'>
        <View className='evaluation-page__product'>
          <Image className='evaluation-page__product-img' src={img} />
          <Text className='evaluation-page__product-name'>{product ? product.name : ''}</Text>
        </View>
        <View className='evaluation-page__star'>
          <View className='evaluation-page__star-title'>请评分：</View>{' '}
          <AtRate value={star} onChange={this.handleStartChange} />
        </View>

        <View className='evaluation-page__details'>
          <View className='evaluation-page__details-title'>写下您的使用体验吧</View>
          <AtTextarea
            className='evaluation-page__details-textarea'
            value={details}
            onChange={this.handleDetailsChange}
          />
        </View>
        <View className='evaluation-page__images'>
          <View className='evaluation-page__images-title'>添加图片</View>

          <AtImagePicker
            multiple
            files={images as any}
            onChange={this.handleImgsChange}
            // onFail={this.onFail}
            onImageClick={this.handleClickImage}
          />
        </View>

        <View className='evaluation-page__submit-btn'>
          <AtButton type='primary' onClick={this.submit}>
            提交
          </AtButton>
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default EvaluationPage
