import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as shopAddressesApi from '../../../apis/shopAddressesApi'

// models
import ShopAddresses from '../../../models/ShopAddresses'

// store
import detailStore from '../detail/store'
import accountStore from '../../../store/account'

class AddressListStore {
  @observable
  addressList: ShopAddresses[] = []

  @observable
  selectMode: boolean = false

  @observable
  selectedId: number | null = null

  @computed
  get selectedAddress() {
    return this.addressList.find(({ id }) => id === this.selectedId) || null
  }

  @action
  async init({ selectMode }: { selectMode: boolean } = { selectMode: false }) {
    Taro.showLoading({ title: '加载中...' })

    this.selectMode = selectMode

    const session = await accountStore.getLoginSession()

    const resp = await shopAddressesApi.getAddrByUserId({ userId: session.uid })
    this.addressList = resp.data

    const defaultAddress = this.addressList.find(({ isDefault }) => isDefault === 1)
    if (!this.selectedId && defaultAddress) {
      this.selectedId = defaultAddress.id
    }

    Taro.hideLoading()
  }

  @action
  navigateToAdd = async () => {
    try {
      Taro.showLoading({ title: '加载中...' })
      const choosedAddress = await Taro.chooseAddress()
      Taro.hideLoading()

      detailStore.navigateTo({
        address: {
          name: choosedAddress.userName,
          mobile: choosedAddress.telNumber,
          region: [choosedAddress.provinceName, choosedAddress.cityName, choosedAddress.countyName],
          address: choosedAddress.detailInfo,
          isDefault: true,
        },
      })
    } catch (error) {
      Taro.hideLoading()
      // 拒绝授权了
      detailStore.navigateTo()
    }
  }
  @action
  navigateToDetail = (address?: ShopAddresses) => {
    if (address) {
      detailStore.navigateTo({
        address: {
          id: address.id,
          name: address.consignee,
          mobile: address.mobile,
          region: [address.provinceName, address.cityName, address.districtnName],
          address: address.address,
          isDefault: address.isDefault === 1 ? true : false,
        },
      })
    } else {
      detailStore.navigateTo()
    }
  }

  @action
  select = (id: number) => {
    this.selectedId = id
    Taro.navigateBack()
  }
}

export default new AddressListStore()
