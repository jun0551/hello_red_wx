import { View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'
import classnames from 'classnames'

// models
import ProductsCategory from '../../../../models/ProductsCategory'

// styles
import './index.scss'

export interface DataProps {
  list: ProductsCategory[]
  currentId?: number
}
export interface EventProps {
  onClick: (item: ProductsCategory) => void
}

export type Props = DataProps & EventProps

interface State {}

class Categorys extends Component<Props, State> {
  // ---------------- 私有属性 ----------------

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {}
  }

  // ---------------- 生命周期 ----------------

  // ---------------- 计算属性 ----------------

  // ---------------- 方法 ----------------
  handleClickItem = (currentItem: ProductsCategory) => {
    this.props.onClick(currentItem)
  }

  render() {
    const { list, currentId } = this.props
    return (
      <View className='categorys'>
        {list && list.map(item => {
          return (
            <View
              key={item.id}
              className={classnames('categorys__item', item.id === currentId && 'categorys__item-active')}
              onClick={this.handleClickItem.bind(this, item)}
            >
              {item.name}
            </View>
          )
        })}
      </View>
    )
  }
}

export default Categorys
