import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// models
import OrderProduct from '../../models/OrderProduct'

// store
import accountStore from '../../store/account'

class OrderListStore {
  tabList = [
    { title: '全部', status: [] },
    { title: '待付款', status: [OrderProduct.Status.PendingPay] },
    { title: '待收货', status: [OrderProduct.Status.PendingDelivery, OrderProduct.Status.PendingReceived] },
    { title: '待评价', status: [OrderProduct.Status.PendingComment] },
    {
      title: '已完成',
      status: [OrderProduct.Status.TransactionSuccess, OrderProduct.Status.Refund, OrderProduct.Status.Cancel],
    },
  ]

  @observable
  currentTab: number = 0

  @observable
  orderList: OrderProduct[] = []

  @computed.struct
  get currentStatus() {
    return this.tabList[this.currentTab].status
  }

  @action
  async init(tabIndex: number) {
    this.currentTab = tabIndex
    this.orderList = []

    this.loadList()
  }

  @action
  tabChange = (value: number) => {
    this.currentTab = value
    this.loadList()
  }

  @action
  async loadList({ showLoading }: { showLoading: boolean } = { showLoading: true }) {
    if (showLoading) Taro.showLoading({ title: '加载中...' })

    const session = await accountStore.getLoginSession()

    const resp = await orderApi.getList({ userId: session.uid, status: this.currentStatus, page: 1, limit: 20 })
    this.orderList = resp.data
    if (showLoading) Taro.hideLoading()
  }

  /**
   * 取消订单
   */
  @action
  async cancelOrder(order: OrderProduct) {
    const result = await Taro.showModal({
      title: '确认',
      content: '确定要取消订单吗？',
      confirmText: '取消订单',
      cancelText: '返回',
    })

    if (!result.confirm) return

    Taro.showLoading({ title: '取消中...' })
    await orderApi.cancelOrder({ orderId: order.id })

    Taro.hideLoading()
    Taro.showToast({ title: '已取消' })

    await this.loadList({ showLoading: false })
  }

  /**
   * 确认收货
   */
  @action
  async confirmReceipt(order: OrderProduct) {
    const result = await Taro.showModal({
      title: '确定',
      content: '确定确认收货？',
    })

    if (!result.confirm) return

    Taro.showLoading({ title: '正在确认收货...' })
    await orderApi.confirmReceipt({ orderId: order.id })

    Taro.hideLoading()
    Taro.showToast({ title: '已确认收货' })

    // TODO: 跳转到评价订单

    await this.loadList({ showLoading: false })
  }
}

export default new OrderListStore()
