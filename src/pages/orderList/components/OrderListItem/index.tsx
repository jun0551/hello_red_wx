import { View, Text, Image } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

// module
import OrderProduct from '../../../../models/OrderProduct'

// styles
import './index.scss'

// store
import orderStore from '../../store'
import submitOrderStore from '../../../submitOrder/store'
import payStore from '../../../pay/store'
import evaluationStore from '../../../evaluation/store'

export interface DataProps {
  order: OrderProduct
}
export interface EventProps {}
export type Props = DataProps & EventProps

interface State {}

class OrderListItem extends Component<Props, State> {
  // ---------------- 私有属性 ----------------

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {}
  }

  // ---------------- 生命周期 ----------------
  componentDidMount() {
    //
  }

  // ---------------- 计算属性 ----------------
  /**
   * 是否可以取消订单
   */
  canCancelBtn() {
    const status = this.props.order.oStatus
    return status === OrderProduct.Status.PendingPay
  }

  /**
   * 是否显示再次购买
   * 待发货、已完成、退款
   */
  isShowBuyAgainBtn() {
    const status = this.props.order.oStatus
    return (
      status === OrderProduct.Status.PendingDelivery ||
      status === OrderProduct.Status.TransactionSuccess ||
      status === OrderProduct.Status.Refund
    )
  }

  /**
   * 取消订单
   */
  cancelOrder = () => {
    orderStore.cancelOrder(this.props.order)
  }

  /**
   * 去支付
   */
  goToPay = () => {
    payStore.navigateTo({ orderId: this.props.order.id })
  }

  /**
   * 查看物流
   */
  viewLogistics = () => {
    console.info('TODO:查看物流')
  }
  /**
   * 确认收货
   */
  confirmReceipt = () => {
    orderStore.confirmReceipt(this.props.order)
  }

  /**
   * 评价
   */
  evaluation = () => {
    const { order } = this.props

    // TODO: 目前订单只有一个商品，直接评论该商品
    const orderProductItem = order.orderProductItems[0]
    if (!orderProductItem) throw new Error('orderProductItem is not exist')
    const productId = orderProductItem.pid

    evaluationStore.navigateTo({ orderId: order.id, productId })
  }

  /**
   * 再次购买
   */
  buyAgain = () => {
    submitOrderStore.buyAngin(this.props.order)
  }

  // ---------------- 方法 ----------------

  render() {
    const { order } = this.props

    const cancelBtn = (
      <View className='order-item__secondary-button' onClick={this.cancelOrder}>
        取消订单
      </View>
    )

    return (
      <View className='order-item'>
        <View className='order-item__top'>
          <Text className='order-item__no'>订单编号：{order.id}</Text>
          <Text className='order-item__status'>{OrderProduct.StatusText[order.oStatus]}</Text>
        </View>
        <View className='order-item__products'>
          {order &&
            order.orderProductItems.map(orderProductItem => {
              const product = orderProductItem.productsInfo
              const imgInfo = product.imageInfos[0] || {}
              // TODO
              const imgUrl = imgInfo.imgUrl || ''

              return (
                <View key={orderProductItem.id} className='order-item__products-item'>
                  <Image className='order-item__products-item-photo' src={imgUrl} />

                  <View className='order-item__products-item-right'>
                    <View className='order-item__products-item-name'>
                      <Text>商品名称</Text>
                      <Text className='order-item__products-item-quantity'>{product.name}</Text>
                    </View>
                    {/* <View className='order-item__products-item-desc'>商品规格</View> */}
                    <View className='order-item__products-item-price'>¥{orderProductItem.singlePrice}</View>
                  </View>
                </View>
              )
            })}
        </View>
        <View className='order-item__bottom'>
          <View className='order-item__bottom-left'>
            <Text className='order-item__total-quantity'>共{order.orderProductItems.length}件商品 需付款:</Text>
            <Text className='order-item__total-price-unit'>¥</Text>
            <Text className='order-item__total-price'>{order.totalPrices}</Text>
          </View>

          {/* 待付款 */}
          {order.oStatus == OrderProduct.Status.PendingPay && (
            <View className='order-item__buttons'>
              {/* 取消订单 */}
              {cancelBtn}
              {/* 去支付 */}
              <View className='order-item__primary-button' onClick={this.goToPay}>
                去支付
              </View>
            </View>
          )}

          {/* 待发货 */}
          {order.oStatus == OrderProduct.Status.PendingDelivery && (
            <View className='order-item__buttons'>
              {/* 再次购买 */}

              <View className='order-item__primary-button' onClick={this.buyAgain}>
                再次购买
              </View>
            </View>
          )}

          {/* 待收货 */}
          {order.oStatus == OrderProduct.Status.PendingReceived && (
            <View className='order-item__buttons'>
              {/* 查看物流  */}
              <View className='order-item__secondary-button'>查看物流</View>
              {/* 确认收货 */}
              <View className='order-item__primary-button' onClick={this.confirmReceipt}>
                确认收货
              </View>
            </View>
          )}

          {/* 待评价 */}
          {order.oStatus == OrderProduct.Status.PendingComment && (
            <View className='order-item__buttons'>
              {/* 立即评价  */}
              <View className='order-item__secondary-button' onClick={this.evaluation}>
                立即评价
              </View>
              {/* 再次购买 */}

              <View className='order-item__primary-button' onClick={this.buyAgain}>
                再次购买
              </View>
            </View>
          )}

          {/* 交易成功 */}
          {order.oStatus == OrderProduct.Status.TransactionSuccess && (
            <View className='order-item__buttons'>
              {/* 再次购买 */}

              <View className='order-item__primary-button' onClick={this.buyAgain}>
                再次购买
              </View>
            </View>
          )}

          {/* 退款 */}
          {order.oStatus == OrderProduct.Status.Refund && (
            <View className='order-item__buttons'>
              {/* 再次购买 */}
              <View className='order-item__primary-button' onClick={this.buyAgain}>
                再次购买
              </View>
            </View>
          )}
        </View>
      </View>
    )
  }
}

export default OrderListItem
