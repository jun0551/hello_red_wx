import Taro, { Component, Config } from '@tarojs/taro'
import { View, ScrollView, Text, Image } from '@tarojs/components'
import { AtTabs } from 'taro-ui-hello-red'

import { observer, inject } from '@tarojs/mobx'

import isNumber from 'lodash/isNumber'

import OrderListItem from './components/OrderListItem'

import './index.scss'

import store from './store'

type DataProps = {
  store: typeof store
}

type EventProps = {}

type State = {
  // currentTab: number
}

type Props = DataProps & EventProps

@inject(() => {
  return { store }
})
@observer
class OrderListPage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '我的订单',
    backgroundColor: '#F3F2ED',
  }

  tabChange = store.tabChange

  constructor(props: Props) {
    super(props)
  }

  componentDidMount() {}

  async componentDidShow() {
    let tabIndex = parseFloat(this.$router.params.tabIndex)

    if (!isNumber(tabIndex)) {
      tabIndex = 0
    }
    store.init(tabIndex)
  }

  render() {
    const { orderList, tabList, currentTab } = this.props.store

    return (
      <View className='order-list-page'>
        <AtTabs current={currentTab} tabList={tabList} onClick={this.tabChange}>
          {/* {tabList.map((_, i) => {
            return <AtTabsPane key={i} current={currentTab} index={i} />
          })} */}
        </AtTabs>

        <ScrollView className='order-list-page__tabs-pane' scrollY>
          {orderList.map(order => {
            return (
              <View key={order.id} className='order-list-page__order-list-item'>
                <OrderListItem order={order} />
              </View>
            )
          })}
        </ScrollView>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default OrderListPage
