import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as feedbackApi from '../../apis/feedbackApi'

// store
import accountStore from '../../store/account'

class FeedbackStore {
  @observable
  content: string = ''

  constructor() {}

  @action
  navigateTo() {
    Taro.navigateTo({
      url: '/pages/feedback/index',
    })
  }

  @action
  init = () => {
    this.content = ''
  }

  @action
  changeContent = (content: string) => {
    this.content = content
  }

  @action
  submit = async () => {
    Taro.showLoading({
      title: '提交中...',
      mask: true,
    })
    const session = await accountStore.getLoginSession()
    await feedbackApi.add({
      userId: session.uid,
      content: this.content,
    })
    Taro.hideLoading()

    Taro.showToast({ title: '提交成功', mask: true, duration: 1500 })

    await new Promise(resolve => setTimeout(resolve, 1500))

    Taro.navigateBack()
  }
}

export default new FeedbackStore()
