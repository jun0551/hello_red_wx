import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'

import { observer, inject } from '@tarojs/mobx'

// components
import { AtTextarea, AtButton } from 'taro-ui-hello-red'

// styles
import './index.scss'

// store
import store from './store'

type DataProps = {
  store: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { store: store }
})
@observer
class FeedbackPage extends Component<Props, State> {
  submit = store.submit
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '意见反馈',
    navigationBarBackgroundColor: '#F2F2F2',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentDidShow() {
    await this.props.store.init()
  }

  handleContentChange = (event: any) => {
    store.changeContent(event.target.value)
  }

  render() {
    const { content } = this.props.store
    return (
      <View className='feedback-page'>
        <View className='feedback-page__content'>
          <AtTextarea
            value={content}
            placeholder='请输入您的反馈意见（字数200以内）'
            maxLength={200}
            count
            onChange={this.handleContentChange}
          />
        </View>
        <View className='feedback-page__submit'>
          <AtButton type='primary' onClick={this.submit}>提交</AtButton>
        </View>
      </View>
    )
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default FeedbackPage
