import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// models
import ProductsInfo from '../../models/ProductsInfo'

// store
import accountStore from '../../store/account'

class CommentStore {
  constructor() {}

  @action
  navigateTo({ orderId, productId }: { orderId: number; productId: number }) {
    Taro.navigateTo({
      url: `/pages/comment/index?orderId=${orderId}`,
    })
  }

  @action
  init = () => {
    // this.checkedList =
  }
}

export default new CommentStore()
