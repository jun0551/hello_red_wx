import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// models
import ProductsInfo from '../../models/ProductsInfo'

// store
import accountStore from '../../store/account'

class PayStore {

  constructor() {
  }


  @action
  navigateTo({ orderId }: { orderId: number }): any {
    Taro.navigateTo({
      url: `/pages/pay/index?orderId=${orderId}`,
    })
  }

  @action
  init = () => {
    // this.checkedList =
  }

}

export default new PayStore()
