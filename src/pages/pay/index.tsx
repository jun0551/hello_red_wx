import Taro, { Component, Config } from '@tarojs/taro'
import { View, Image, Text } from '@tarojs/components'

import { observer, inject } from '@tarojs/mobx'

// components
// import { AtTextarea, AtCheckbox, AtButton } from 'taro-ui-hello-red'

// styles
import './index.scss'

// store
import store from './store'

type DataProps = {
  store: typeof store
}

type EventProps = {}

type State = {}

type Props = DataProps & EventProps

@inject(() => {
  return { store: store }
})
@observer
class PayPage extends Component<Props, State> {
  /**
   * 指定config的类型声明为: Taro.Config
   *
   * 由于 typescript 对于 object 类型推导只能推出 Key 的基本类型
   * 对于像 navigationBarTextStyle: 'black' 这样的推导出的类型是 string
   * 提示和声明 navigationBarTextStyle: 'black' | 'white' 类型冲突, 需要显示声明类型
   */
  config: Config = {
    navigationBarTitleText: '确认支付',
    navigationBarBackgroundColor: '#F2F2F2',
  }

  constructor(props: Props) {
    super(props)
  }

  async componentDidShow() {
    await this.props.store.init()
  }

  render() {
    return <View className='pay-page'>支付中...</View>
  }
}

// #region 导出注意
//
// 经过上面的声明后需要将导出的 Taro.Component 子类修改为子类本身的 props 属性
// 这样在使用这个子类时 Ts 才不会提示缺少 JSX 类型参数错误
//
// #endregion

export default PayPage
