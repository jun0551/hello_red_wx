import Taro from '@tarojs/taro'
import { action, observable, computed } from 'mobx'

// apis
import * as orderApi from '../../apis/orderApi'

// helper
import { CheckToRadioState } from '../../helper/checkToRadioState'

// models
import ProductsInfo from '../../models/ProductsInfo'
import OrderProduct from '../../models/OrderProduct'

// store
import addressListStore from '../address/list/store'
import payStore from '../pay/store'
import accountStore from '../../store/account'

export enum CheckboxValue {
  Address = 'address',
  Self = 'self',
}

export enum PayCheckboxValue {
  WeiXin = 'Weixin',
  ZhiFuBao = 'ZhiFuBao',
}

class SubmitOrderStore {
  @observable
  remark: string = '' // 备注

  @observable
  product?: ProductsInfo // 商品

  @observable
  productCount: number = 0 // 商品购买数量

  @observable
  checkboxState: CheckToRadioState<CheckboxValue>
  @observable
  payCheckboxState: CheckToRadioState<PayCheckboxValue> = new CheckToRadioState({
    value: PayCheckboxValue.WeiXin,
  })

  constructor() {
    this.checkboxState = new CheckToRadioState({
      value: CheckboxValue.Address,
      onChange: (value, prevValue) => {
        if (value === prevValue && value === CheckboxValue.Address) {
          Taro.navigateTo({ url: '/pages/address/list/index?selectMode=1' })
        }
      },
    })
  }

  @computed.struct
  get checkboxOptions() {
    const addressOption = {
      value: CheckboxValue.Address,
      label: '收货地址',
      arrow: 'right',
    }

    if (addressListStore.selectedAddress) {
      addressOption.label = `${addressListStore.selectedAddress.consignee}`
    }

    return [
      addressOption,
      {
        value: CheckboxValue.Self,
        label: '自提',
      },
    ]
  }

  @computed.struct
  get currentCheckedList() {
    return this.checkboxState.checkedList
  }

  @computed.struct
  get payCheckboxOptions() {
    return [
      {
        value: PayCheckboxValue.WeiXin,
        label: '微信支付',
        thumb: require('../../assets/icons/weixin.png'),
      },
      // {
      //   value: PayCheckboxValue.ZhiFuBao,
      //   label: '支付宝'
      // }
    ]
  }
  @computed.struct
  get payCheckedList() {
    return this.payCheckboxState.checkedList
  }

  @computed.struct
  get checkboxValue() {
    return this.checkboxState.value
  }

  @action
  navigateTo = ({ product, buyQuantity }: { product: ProductsInfo; buyQuantity: number }) => {
    this.product = product
    this.productCount = buyQuantity

    Taro.navigateTo({
      url: '/pages/submitOrder/index',
    })
    addressListStore.init()
  }

  /**
   * 再次购买
   */
  @action
  buyAngin = (order: OrderProduct) => {
    const orderProductItem = order.orderProductItems[0]
    if (!orderProductItem) throw new Error('order product item is not exist')

    const product = order.orderProductItems[0].productsInfo
    const buyQuantity = orderProductItem.buyCount

    this.navigateTo({
      product,
      buyQuantity,
    })
  }

  @action
  changeRemark = (value: string) => {
    this.remark = value
  }

  @action
  handleCheckboxChange = (value: CheckboxValue[]) => {
    this.checkboxState.change(value)
  }

  @action
  handlePayCheckboxChange = (value: PayCheckboxValue[]) => {
    return this.payCheckboxState.change(value)
  }

  @action
  submitPay = async () => {
    if (!this.product) throw new Error('product is not exist')

    await Taro.showLoading({
      title: '加载中...',
      mask: true,
    })

    const loginSession = await accountStore.getLoginSession()

    const resp = await orderApi.addOrder({
      productId: this.product.id,
      num: this.productCount,
      userId: loginSession.uid,
      totalPrices: this.product.shoppPrice * this.productCount,
    })

    const order = resp.data

    Taro.hideLoading()

    payStore.navigateTo({ orderId: order.id })
  }
}

export default new SubmitOrderStore()
