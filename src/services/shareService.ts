import Taro from '@tarojs/taro'

export const share = async () => {
  await Taro.showShareMenu()
}

export const onShareAppMessage = (obj: Taro.ShareAppMessageObject) => {
  return {
    title: '红品',
    path: '/pages/home/index',
    imageUrl: '',
  }
}
