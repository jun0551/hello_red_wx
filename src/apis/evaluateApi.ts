import { createApi, BaseData } from '../helper/api'

// models
import Evaluate from '../models/Evaluate'

/**
 * 评论
 */
export namespace evaluate {
  export interface Param {
    productId: number
    userId: number
    orderId: number
    star: number
    details: string
    url: string
  }
  export type Response = BaseData<Evaluate>
}
export const evaluate = createApi<[evaluate.Param], evaluate.Response>(param => ({
  url: '/evaluate/insert',
  method: 'POST',
  data: {
    pid: param.productId,
    uid: param.userId,
    oid: param.orderId,
    star: param.star,
    details: param.details,
    url: param.url,
  },
}))
