import { createApi, ListData } from '../helper/api'
import PageParams from './types/PageParams'

// models
import ProductsCategory from '../models/ProductsCategory'

/**
 * 根据栏目ID查询分类列表
 */
export namespace getListByColumnId {
  export type Param = PageParams & { columnId: number }
  export type Response = ListData<ProductsCategory>
}
export const getListByColumnId = createApi<[getListByColumnId.Param], getListByColumnId.Response>((param) => ({
  url: '/category/listByColumn',
  method: 'POST',
  data: {
    columnID: param.columnId
  }
}))
