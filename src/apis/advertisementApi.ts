import { createApi, ListData } from '../helper/api'

// models
import Advertisement from '../models/Advertisement'

/**
 * 获取广告首页数据
 */
export namespace getList {
  export interface Param {
    type: Advertisement.Type
  }
  export type Response = ListData<Advertisement>
}
export const getList = createApi<[getList.Param], getList.Response>(param => ({
  url: '/advertisement/list',
  method: 'POST',
  data: param,
}))
