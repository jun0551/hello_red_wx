import { createApi, ListData, BaseData } from '../helper/api'

// models
import OrderProduct from '../models/OrderProduct'

/**
 * 根据分类ID查询商品数据
 */
export namespace getList {
  export type Param = {
    userId: number
    status?: number[]
    page?: number
    limit?: number
  }

  export type Response = ListData<OrderProduct>
}
export const getList = createApi<[getList.Param], getList.Response>(param => ({
  url: '/order/get',
  method: 'POST',
  data: {
    userID: param.userId,
    status: param.status,
    page: param.page,
    limit: param.limit,
  },
}))

/**
 * 根据分类ID查询商品数据
 */
export namespace getCount {
  export type Param = {
    userId: number
  }
  export interface ResponseData {
    dfk: number // 待付款
    dfh: number // 待发货
    dsh: number //待收货
    dpj: number //待评价
    jycg: number //交易成功
    tk: number //退款
    ygb: number //已关闭
  }

  export type Response = BaseData<ResponseData>
}
export const getCount = createApi<[getCount.Param], getCount.Response>(param => ({
  url: '/order/getOrderCnt',
  method: 'POST',
  data: {
    userID: param.userId,
  },
}))

/**
 * 订单生成
 */
export namespace addOrder {
  export type Param = {
    productId: number
    userId: number
    num: number
    totalPrices: number // TODO: 不应该前端传给后端，待接口修改再处理
  }

  export type Response = BaseData<OrderProduct>
}
export const addOrder = createApi<[addOrder.Param], addOrder.Response>(param => ({
  url: '/order/addOrder',
  method: 'POST',
  data: param,
}))

/**
 * 取消订单
 */
export namespace cancelOrder {
  export type Param = {
    orderId: number
  }

  export type Response = BaseData<OrderProduct>
}
export const cancelOrder = createApi<[cancelOrder.Param], cancelOrder.Response>(param => ({
  url: '/order/cancelOrder',
  method: 'POST',
  data: param,
}))

/**
 * 确认收货
 */
export namespace confirmReceipt {
  export type Param = {
    orderId: number
  }

  export type Response = BaseData<OrderProduct>
}
export const confirmReceipt = createApi<[confirmReceipt.Param], confirmReceipt.Response>(param => ({
  url: '/order/checkOrder',
  method: 'POST',
  data: param,
}))
