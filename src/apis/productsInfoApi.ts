import { createApi, ListData, BaseData } from '../helper/api'

// models
import ProductsInfo from '../models/ProductsInfo'

/**
 * 根据分类获取商品
 */
export namespace getByCategoryId {
  export interface Param {
    categoryId: number
    page: number
    limit: number
  }
  export type Response = ListData<ProductsInfo>
}
export const getByCategoryId = createApi<[getByCategoryId.Param], getByCategoryId.Response>(param => ({
  url: `/productsInfo/getByCategoryId/${param.categoryId}`,
  method: 'POST',
  data: {
    page: param.page,
    limit: param.limit
  }
}))

/**
 * 获取id获取商品
 */
export namespace getById {
  export interface Param {
    id: number
  }
  export type Response = BaseData<ProductsInfo>
}
export const getById = createApi<[getById.Param], getById.Response>(param => ({
  url: `/productsInfo/get/${param.id}`,
  method: 'POST'
}))
