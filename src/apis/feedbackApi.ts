import { createApi, BaseData } from '../helper/api'

/**
 * 评论
 */
export namespace add {
  export interface Param {
    userId: number
    content: string
  }
  export type Response = BaseData<void>
}
export const add = createApi<[add.Param], add.Response>(param => ({
  url: '/feedBack/add',
  method: 'POST',
  data: {
    uid: param.userId,
    content: param.content,
  },
}))
