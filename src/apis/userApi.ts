import { createApi, BaseData } from '../helper/api'

// models
import UserAccount from '../models/UserAccount'

/**
 * 根据id获取用户信息
 */
export namespace queryById {
  export interface Param {
    userId: number
  }
  export type Response = BaseData<UserAccount>
}
export const queryById = createApi<[queryById.Param], queryById.Response>(param => ({
  url: `/user/get/${param.userId}`,
  method: 'GET',
}))

export namespace updateUserInfo {
  export interface Param {
    userId: number
    nickName: string
    avatarUrl: string
  }
  export type Response = BaseData<UserAccount>
}
export const updateUserInfo = createApi<[updateUserInfo.Param], updateUserInfo.Response>(param => ({
  url: `/user/updateUserInfo`,
  method: 'POST',
  data: {
    id: param.userId,
    nickName: param.nickName,
    url: param.avatarUrl,
  },
}))
