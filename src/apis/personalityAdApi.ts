import { createApi, ListData } from '../helper/api'

// models
import PersonalityAd from '../models/PersonalityAd'

/**
 * 获取推荐页面数据
 */
export namespace getList {
  export type Response = ListData<PersonalityAd>
}
export const getList = createApi<getList.Response>(() => ({
  url: '/personalityAd/list',
  method: 'GET'
}))
