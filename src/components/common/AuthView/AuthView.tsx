import { View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'
import classNames from 'classnames'

import config from '../../../config'

import uploadFile from '../../../helper/uploadFile'

// styles
import './AuthView.scss'

// components
import Authorize from '../AuthorizeInfo'

// models

// store
import accountStore from '../../../store/account'

// services

// api
// import * as miniProgramApi from '../../apis/miniProgramApi'

export interface DataProps {}
export interface EventProps {
  onReady?: () => any
}
export type Props = DataProps & EventProps

interface State {
  loading: boolean
  isOpened: boolean
  miniProgramInfo: any
}
export default class AuthView extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      loading: true,
      isOpened: false,
      miniProgramInfo: {},
    }
  }

  componentDidMount() {
    this.wxAuthorize()
  }

  // 检查是否已经微信授权
  wxAuthorize = async () => {
    Taro.showLoading({ title: '加载中...', mask: true })

    const res = await Taro.getSetting()
    // 是否授权过，同意或拒绝
    const hasAuth = 'scope.userInfo' in res.authSetting
    // 是否已同意
    const isAgree = res.authSetting['scope.userInfo']
    let needAuth = !hasAuth

    // 非线上时，未授权或已拒绝都提示用户授权，为了方便调试
    if (!config.online) {
      needAuth = !isAgree
    }

    if (needAuth) {
      // config.appId
      this.setState({
        isOpened: true,
        loading: false,
      })
      Taro.hideTabBar({ animation: false })
      Taro.hideLoading()
      return
    }

    this.setState(
      {
        isOpened: false,
        loading: false,
      },
      async () => {
        this.ready()
      },
    )
  }

  // 点击完成微信授权
  onClickAuthorize = async (userInfo: Authorize.UserInfo) => {
    Taro.showLoading({ title: '加载中...', mask: true })

    await this.updateInfo(userInfo)

    Taro.hideLoading()

    this.setState(
      {
        isOpened: false,
      },
      () => {
        this.ready()
      },
    )
  }

  /**
   * 更新资料
   */
  async updateInfo(userInfo: Authorize.UserInfo) {
    console.info('TODO:保存头像、昵称', userInfo)

    const session = await accountStore.getLoginSession()
    const updateUserInfo = {
      nickName: `游客_${session.uid}`,
      avatarUrl: '',
    }
    if (userInfo) {
      // TODO: 头像先上传到七牛
      // const imageInfo = await Taro.getImageInfo({ src: userInfo.avatarUrl })

      // const resp = await uploadFile({
      //   name: 'file',
      //   filePath: imageInfo.path,
      // })
      // console.info('TODO:上传图片，然后保存头像', imageInfo, resp)
      updateUserInfo.nickName = userInfo.nickName
      updateUserInfo.avatarUrl = userInfo.avatarUrl
    }

    await accountStore.updateAccoun(updateUserInfo)
  }

  /**
   * 准备好
   */
  async ready() {
    Taro.showTabBar({ animation: false })

    if (this.props.onReady) {
      this.props.onReady()
    }
  }

  render() {
    const { isOpened, loading } = this.state

    return (
      <View
        className={classNames('auth-view', {
          'auth-view__show-auth': isOpened || loading,
        })}
      >
        {isOpened && (
          <Authorize onClickAuthorize={this.onClickAuthorize} miniProgramInfo={this.state.miniProgramInfo} />
        )}
        {this.props.children}
      </View>
    )
  }
}
