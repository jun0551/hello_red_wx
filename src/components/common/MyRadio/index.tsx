import { View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'
import classnames from 'classnames'

import { AtIcon } from 'taro-ui-hello-red'

import { calcPx } from '../../../helper/px'

import './index.scss'

export interface DataProps {
  checked: boolean
}
export interface EventProps {
  onChange?: (checked: boolean) => void
}
export type Props = DataProps & EventProps

interface State {}

class MyRadio extends Component<Props, State> {
  // ---------------- 私有属性 ----------------

  // ---------------- 生命周期 ----------------

  // ---------------- 计算属性 ----------------

  handleClick = () => {
    if (this.props.onChange) {
      this.props.onChange(!this.props.checked)
    }
  }

  render() {
    const { checked } = this.props
    return (
      <View className={classnames('my-radio', checked && 'my-radio__checked')} onClick={this.handleClick}>
        <View className='my-radio___container'>
          <AtIcon value='check' size='14' color='#fff' />
        </View>
      </View>
    )
  }
}

export default MyRadio
