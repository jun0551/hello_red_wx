import { Button, Image, View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

import './AuthorizeInfo.scss'

interface State {}

namespace AuthorizeInfo {
  export type UserInfo = Taro.getUserInfo.Promised['userInfo'] | undefined

  export interface DataProps {
    miniProgramInfo: any
  }
  export interface EventProps {
    /**
     * 拒绝授权返回undefined
     */
    onClickAuthorize: (userInfo: AuthorizeInfo.UserInfo) => void
  }
  export type Props = DataProps & EventProps
}
class AuthorizeInfo extends Component<AuthorizeInfo.Props, State> {
  constructor(props: AuthorizeInfo.Props) {
    super(props)
    this.state = {}
  }

  componentWillMount() {
    //
  }

  // 授权成功事件
  handleConfirm = async (res: any) => {
    // 拒绝授权时，userInfo === undefined
    const userInfo: AuthorizeInfo.UserInfo = res.detail.userInfo

    this.props.onClickAuthorize(userInfo)
  }

  render() {
    return (
      <View className='wx-authorize'>
        <View>
          <View>
            <Image
              className='yi-icon'
              src={this.props.miniProgramInfo.headImg}
            />
          </View>
          <View className='project-name'>{this.props.miniProgramInfo.name}</View>
          <View className='tip'>请完成微信授权以继续使用</View>
          <View className='footer-btn'>
            <Button
              openType='getUserInfo'
              type='primary'
              onGetUserInfo={this.handleConfirm}
            >
              授权微信用户信息
            </Button>
          </View>
        </View>
      </View>
    )
  }
}
export default AuthorizeInfo
