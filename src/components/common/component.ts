import Taro, { Component } from '@tarojs/taro'

const objectToString = (style: any) => {
  if (style && typeof style === 'object') {
    let styleStr = ''
    Object.keys(style).forEach(key => {
      const lowerCaseKey = key.replace(/([A-Z])/g, '-$1').toLowerCase()
      styleStr += `${lowerCaseKey}:${style[key]};`
    })
    return styleStr
  } else if (style && typeof style === 'string') {
    return style
  }
  return ''
}

export default class AtComponent<Props> extends Component<Props> {
  static options = {
    addGlobalClass: true
  }

  /**
   * 合并 style
   * @param {Object|String} style1
   * @param {Object|String} style2
   * @returns {String}
   */
  mergeStyle(style1: any, style2: any) {
    return objectToString(style1) + objectToString(style2)
  }

  getClassName(arg: any) {
    const { className } = this.props as any

    if (!className) {
      return arg
    }

    let componentClass = arg
    let propsClass = className

    if (!Array.isArray(propsClass)) {
      propsClass = [propsClass]
    }

    if (!Array.isArray(componentClass)) {
      componentClass = [componentClass]
    }

    return componentClass.concat(propsClass)
  }
}
