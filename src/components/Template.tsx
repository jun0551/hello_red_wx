import { View } from '@tarojs/components'
import Taro, { Component } from '@tarojs/taro'

export interface DataProps {
  firstName: string
  lastName: string
}
export interface EventProps {
  onHello: () => void
}
export type Props = DataProps & EventProps

interface State {
  isShow: boolean
}

class Template extends Component<Props, State> {
  // ---------------- 私有属性 ----------------
  /**
   * x
   */
  x: number

  constructor(props: Props) {
    super(props)

    /**
     * state 默认值
     */
    this.state = {
      isShow: false
    }
  }

  // ---------------- 生命周期 ----------------
  componentDidMount() {
    //
  }

  // ---------------- 计算属性 ----------------
  /**
   * full name
   */
  fullName = () => {
    return this.props.firstName + this.props.lastName
  }

  // ---------------- 方法 ----------------
  /**
   * hello
   */
  hello = () => {
    this.props.onHello()
  }

  render() {
    const { isShow } = this.state
    return (
      <View>
        {isShow && <View>{this.fullName()}</View>}
        <View>{this.props.children}</View>
      </View>
    )
  }
}

export default Template
