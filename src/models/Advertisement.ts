namespace Advertisement {
  export enum Type {
    /**
     * 首页广告
     */
    Index = 0,
    /**
     * 分类广告
     */
    Column = 1,
  }
  export enum TargetType {
    /**
     * 商品
     */
    Product = 0,
    /**
     * 店铺
     */
    Store = 1,
  }

  export const TargetTypeText = {
    [TargetType.Product]: '商品',
    [TargetType.Store]: '店铺',
  }
}
interface Advertisement {
  id: number

  title: string

  status: number

  img: string

  targetType: Advertisement.TargetType

  targetId: number

  createTime: number

  type: Advertisement.Type
}

export default Advertisement
