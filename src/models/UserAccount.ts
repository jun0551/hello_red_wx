/**
 * 用户信息(账户相关)
 */
export default interface UserAccount {
  uid: number
  /**父级ID*/
  parentID: number
  /**用户名*/
  userName: string
  /**昵称*/
  nickName: string
  /**头像*/
  avatar: string
  /**安全问题*/
  safeques: string
  /**安全问题答案*/
  safeAnswer: string
  /**是否锁定*/
  locked: number
  /**最后访问时间*/
  lastVisitTime: number
  /**注册时间*/
  registerTime: number
}
