import OrderProductItems from './OrderProductItems'

namespace OrderProduct {
  export enum Status {
    /**
     * 待付款
     */
    PendingPay = 1,
    /**
     * 待发货
     */
    PendingDelivery = 2,
    /**
     * 待收货
     */
    PendingReceived = 3,
    /**
     * 待评价
     */
    PendingComment = 4,
    /**
     * 交易成功
     */
    TransactionSuccess = 5,
    /**
     * 退款
     */
    Refund = 6,
    /**
     * 已取消
     */
    Cancel = 7,
  }

  export const StatusText = {
    [Status.PendingPay]: '待付款',
    [Status.PendingDelivery]: '待发货',
    [Status.PendingReceived]: '待收货',
    [Status.PendingComment]: '待评价',
    [Status.TransactionSuccess]: '交易成功',
    [Status.Refund]: '退款',
    [Status.Cancel]: '已取消',
  }
}

interface OrderProduct {
  id: number
  /**用户ID*/
  uid: number
  /**支付方式 1微信 2储蓄卡 3信用卡*/
  payment: number
  /**订单时间*/
  orderTime: number
  /**总价*/
  totalPrices: number
  /**运费*/
  freight: number
  /**订单状态（待付款，待发货，待收货，待评价，交易成功、退款）*/
  oStatus: OrderProduct.Status
  /**买家留言*/
  leaveMessage: string

  orderProductItems: OrderProductItems[]
}

export default OrderProduct
