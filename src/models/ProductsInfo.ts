import ImageInfo from './ImageInfo'

export default interface ProductsInfo {
  id: number
  /**商户id*/
  mid: number
  /**商品名称*/
  name: string
  /**商品标题*/
  title: string
  /**优惠信息*/
  favorable: string
  /**商品简介*/
  synopsis: string
  /**状态：0上架 1下架*/
  pStatus: number
  /**类别id*/
  categoryID: number
  /**商品销售价格*/
  shoppPrice: number
  /**商品成本价*/
  costpPrice: number
  /**是否为精品*/
  isBest: number
  /**是否热销*/
  isHot: number
  /**是否是新品*/
  isNew: number
  /**是否免运费*/
  isFree: number
  /**重量*/
  weight: number
  /**尺寸*/
  volume: number
  /**计价单位（件重尺）*/
  payType: string
  /**商品库存*/
  inventory: number
  /**警告库存*/
  warnInventory: number
  /**销量*/
  sell: number
  /**好评数*/
  bestCount: number
  /**中等评价数*/
  mediumcCount: number
  /**差评数*/
  badcCount: number
  /**创建时间*/
  createTime: number
  /**图片信息 */
  imageInfos: ImageInfo[]
  /**图片信息 */
  img: string
}
