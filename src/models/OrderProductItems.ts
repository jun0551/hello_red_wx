import ProductsInfo from './ProductsInfo'

export default interface OrderProductItems {
  id: number
  /**订单ID*/
  oid: number
  /**商品ID*/
  pid: number
  /**购买数量*/
  buyCount: number
  /**购买单价 */
  singlePrice: number

  /**
   * 商品信息
   */
  productsInfo: ProductsInfo
}
