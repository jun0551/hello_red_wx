import ImageInfo from './ImageInfo'

export default interface Evaluate {
  id: number
  /**订单ID*/
  oid: number
  /**商品ID*/
  pid: number
  /**用户ID*/
  uid: number

  star: number

  details: string

  edate: number

  /**
   * 评论图
   */
  imageInfoLis: ImageInfo[]
}
