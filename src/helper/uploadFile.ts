import Taro from '@tarojs/taro'

import config from '../config'

export default async (param: Omit<Taro.uploadFile.Param, 'url'>) => {
  return await Taro.uploadFile({
    url: `${config.apiUrl}/sysOss/upload`,
    ...param,
  })
}
